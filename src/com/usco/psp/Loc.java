package com.usco.psp;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Loc {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Loc lc = new Loc();

		lc.mostrarMenu();
		// lc.copiarCotenido();
	}

	public Object leerJavaFile() {
		TestClass prueba = new TestClass();
		return prueba;
	}

	public void contarMetodo() {

		Object prueba = leerJavaFile();

		Class className = prueba.getClass();

		Method[] methods = className.getDeclaredMethods();

		textoDentroMetodo("M�todos", className.getSimpleName(), methods.length, className);

		System.out.println("Lista de m�todos por nombre cualificado\n");
		for (int i = 0; i < methods.length; i++) {
			System.out.print("    ");
			System.out.println(i + 1 + ". " + methods[i]);
		}

		System.out.println("\nLista de m�todos por nombre simple\n");

		/* Get names from Methods */
		ArrayList<String> methodNames = new ArrayList<>();
		for (Method method : methods) {

			methodNames.add(method.getName());
		}

		/* Sort and print names */
		Collections.sort(methodNames);
		int b = 1;
		for (String name : methodNames) {
			System.out.print("    ");
			System.out.println(b + ". " + name);
			b += 1;
		}
	}

	public void contarAtributos() {

		Object prueba = leerJavaFile();

		Class className = prueba.getClass();

		Field[] atributos = className.getDeclaredFields();

		textoDentroMetodo("Atributos", className.getSimpleName(), atributos.length, className);

		for (int i = 0; i < atributos.length; i++) {
			System.out.print("    ");
			System.out.println(i + 1 + ". " + atributos[i].getGenericType() + " - Nombre de la variable: "
					+ atributos[i].getName());
		}
	}

	public void contarArgumentos() {

		Object prueba = leerJavaFile();

		Class className = prueba.getClass();

		Class[] methods = className.getDeclaredClasses();

		textoDentroMetodo("Argumentos(par�metros)", className.getSimpleName(), methods.length, className);

		for (int i = 0; i < methods.length; i++) {
			System.out.print("    ");
			System.out.println(i + 1 + ". " + methods[i]);
		}

	}

	// Para clases
	public void contarClases() {

		Object prueba = leerJavaFile();

		Class className = prueba.getClass();

		Class[] clases = className.getClasses();

		textoDentroMetodo("Clases", className.getSimpleName(), clases.length, className);

		for (int i = 0; i < clases.length; i++) {
			System.out.print("    ");
			System.out.println(i + 1 + ". " + clases[i]);
		}
	}

	public void contarLineas() {

		File file = new File("./src/com/usco/psp/TestClass.java");

		if (file.exists()) {
			try {
				FileReader fr = new FileReader(file);
				LineNumberReader lr = new LineNumberReader(fr);
				BufferedReader br = new BufferedReader(fr);

				int lineNumberCount = 0;
				try {
					while (lr.readLine() != null) {
						lineNumberCount++;
					}
					System.out.println("Total de l�neas en el archivo "+file.getName()+": " + lineNumberCount);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void textoDentroMetodo(String nombreEjercicio, String getSimpleName, int clasesLength, Class className) {
		System.out
				.println("Lista de " + nombreEjercicio + " en el archivo " + getSimpleName + ".java = " + clasesLength);
		System.out.println("Nombre cualificado del archivo: " + className + "\n");

	}

	public void mostrarMenu() {
		String opcionLin[] = { "M�todos", "Atributos y Argumetos", "Clases", "L�neas de c�digo",
				"Todos los lineamientos" };
		System.out.println("Elija la opci�n a evaluar:\n");

		for (int i = 0; i < opcionLin.length; i++) {
			System.out.println(i + 1 + ". Identificar " + opcionLin[i]);
		}

		System.out.println();
		Scanner myInput = new Scanner(System.in);
		int opcion = myInput.nextInt();

		System.out.println();

		switch (opcion) {
		case 1:
			System.out.println(".............");
			contarMetodo();
			System.out.println();
			break;
		case 2:
			System.out.println(".............");
			contarAtributos();
			System.out.println();
			System.out.println(".............");
			contarArgumentos();
			System.out.println();
			break;
		case 3:
			System.out.println(".............");
			contarClases();
			System.out.println();
			break;
		case 4:
			System.out.println(".............");
			contarLineas();
			System.out.println();
			break;
		case 5:
			System.out.println(".............");
			contarMetodo();
			System.out.println();
			System.out.println(".............");
			contarAtributos();
			System.out.println();
			System.out.println(".............");
			contarArgumentos();
			System.out.println();
			System.out.println(".............");
			contarClases();
			System.out.println();
			System.out.println(".............");
			contarLineas();
			System.out.println();
			break;
		default:
			System.out.println("Opci�n no disponible");
		}

	}

	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

	public void copiarCotenido() {
		FileInputStream instream = null;
		FileOutputStream outstream = null;

		try {
			File infile = new File("D://Test.java");
			// File outfile =new File("./src/test.txt");
			File outfile = new File("./src/com/usco/psp/TestClass.java");

			instream = new FileInputStream(infile);
			outstream = new FileOutputStream(outfile);

			byte[] buffer = new byte[1024];

			int length;
			/*
			 * copying the contents from input stream to output stream using read and write
			 * methods
			 */
			while ((length = instream.read(buffer)) > 0) {
				outstream.write(buffer, 0, length);
			}

			// Closing the input/output file streams
			instream.close();
			outstream.close();

			System.out.println("File copied successfully!!");

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
