package com.usco.psp;

public class TestClass {
	
	int entero1;
	int entero2;
	long long1;
	double double1;
	float prueba;
	
	
	
	public void method1() {
		// M�todo 1
	}
	
	public void method2() {
		// M�todo 2
	}
	
	public void method3() {
		// M�todo 3
	}
	
	public void method3(int numero) {
		// M�todo 3 - sobrecarga int
	}
	
	public void method3(String cadena, double doble) {
		// M�todo 3 - sobrecarga String, double
	}
	
	public void method4() {
		// M�todo 4
	}
	
	int method4(String abc){
		// M�todo sobrecargado
		return 1;
	}
	
	public void method5() {
		// M�todo 5
	}
	
	public void method6() {
		// M�todo 6
	}
	
	public void methodNuevoDePruebaViernes() {
		// M�todo Viernes Agosto
	}
	
	public class Clase3{
		
	}
	
	public class Clase4{
		
	}
	
	public class Clase5{
		
	}

}



class Clase2{
	
}